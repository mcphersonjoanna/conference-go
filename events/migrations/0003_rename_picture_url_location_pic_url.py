# Generated by Django 4.0.3 on 2023-06-23 21:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_location_picture_url'),
    ]

    operations = [
        migrations.RenameField(
            model_name='location',
            old_name='picture_url',
            new_name='pic_url',
        ),
    ]
