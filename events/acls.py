import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_pic(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"query": city + " " + state, "per_page": 1}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=params)
    try:
        pic_url = json.loads(response.content)["photos"][0]["url"]
    except IndexError:
        pic_url = None
    return pic_url


def get_weather(city_name, state_code):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city_name},{state_code},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    try:
        lat = json.loads(response.content)[0]["lat"]
        lon = json.loads(response.content)[0]["lon"]
    except IndexError:
        return None

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    try:
        description = json.loads(response.content)["weather"][0]["description"]
        temp = json.loads(response.content)["main"]["temp"]
        weather = {"temp": temp, "description": description}

    except IndexError:
        return None

    return weather
